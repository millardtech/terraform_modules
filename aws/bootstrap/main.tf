resource "aws_dynamodb_table" "state-locks" {
  name         = "${var.lock_prefix}-${var.environment}-${md5(data.aws_caller_identity.current.account_id)}-tflock"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "LockID"

  attribute {
    name = "LockID"
    type = "S"
  }
}

resource "aws_s3_bucket" "shared_state_bootstrapbucket" {
  bucket = "tfstate-${var.environment}-${md5(data.aws_caller_identity.current.account_id)}"
  acl    = "private"

  versioning {
    enabled = true
  }
}

data "aws_iam_policy_document" "shared-state-policy-document" {
  statement {
    principals {
      type        = "AWS"
      identifiers = var.shared_ro_bucket_principals
    }

    actions = [
      "s3:getObject",
    ]

    resources = [
      "arn:aws:s3:::tfstate-${var.environment}-${md5(data.aws_caller_identity.current.account_id)}/*.tfstate",
    ]
  }

  statement {
    principals {
      type        = "AWS"
      identifiers = flatten([var.shared_ro_bucket_principals, var.production_terraform_ro_bucket_principals])
    }

    actions = [
      "s3:listBucket",
    ]

    resources = [
      "arn:aws:s3:::tfstate-${var.environment}-${md5(data.aws_caller_identity.current.account_id)}",
    ]
  }

  statement {
    principals {
      type        = "AWS"
      identifiers = var.shared_bucket_principals
    }

    actions = [
      "s3:*",
    ]

    resources = [
      "arn:aws:s3:::tfstate-${var.environment}-${md5(data.aws_caller_identity.current.account_id)}",
      "arn:aws:s3:::tfstate-${var.environment}-${md5(data.aws_caller_identity.current.account_id)}/*"
    ]
  }

  statement {
    sid    = "ForceTLS"
    effect = "Deny"
    principals {
      type = "AWS"
      identifiers = [
        "*",
      ]
    }
    actions = [
      "s3:*",
    ]
    condition {
      test     = "Bool"
      values   = ["false"]
      variable = "aws:SecureTransport"
    }
    resources = [
      "arn:aws:s3:::tfstate-${var.environment}-${md5(data.aws_caller_identity.current.account_id)}",
      "arn:aws:s3:::tfstate-${var.environment}-${md5(data.aws_caller_identity.current.account_id)}/*"
    ]
  }
}

resource "aws_s3_bucket_policy" "shared-state-policy" {
  bucket = aws_s3_bucket.shared_state_bootstrapbucket.id
  policy = data.aws_iam_policy_document.shared-state-policy-document.json
}

data "aws_region" "current" {
}

data "aws_caller_identity" "current" {
}
