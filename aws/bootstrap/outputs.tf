output "bootstrap_bucket_id" {
  value = aws_s3_bucket.shared_state_bootstrapbucket.id
}

output "table-id" {
  value = aws_dynamodb_table.state-locks.id
}
