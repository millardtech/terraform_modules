variable "environment" {}

variable "shared_bucket_principals" {
  type = list(string)
}

variable "shared_ro_bucket_principals" {
  type = list(string)
}

variable "production_terraform_ro_bucket_principals" {
  type = list(string)
}

variable "lock_prefix" {
  default = "terra"
}
