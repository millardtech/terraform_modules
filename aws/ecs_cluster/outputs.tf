output "ecs_cluster_id" {
  value = aws_ecs_cluster.cluster.id
}

output "ecs_cluster_name" {
  value = aws_ecs_cluster.cluster.name
}

output "ecs_instance_role_id" {
  value = aws_iam_role.ecsInstanceRole.id
}

output "ecs_instance_profile_id" {
  value = aws_iam_instance_profile.ecsInstanceProfile.id
}

output "ecs_instance_profile_arn" {
  value = aws_iam_instance_profile.ecsInstanceProfile.arn
}

output "ecsServiceRole_arn" {
  value = length(aws_iam_role.ecsServiceRole) > 0 ? aws_iam_role.ecsServiceRole[0].arn : null
}

output "ecsInstanceRole_arn" {
  value = aws_iam_role.ecsInstanceRole.arn
}
