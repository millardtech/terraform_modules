# aws/ecs/cluster - EC2 Container Service Cluster
This module is used to create an ECS cluster along with the necessary
IAM roles to function.

## What this does

 - Create ECS cluster named after var.cluster
 - Create IAM roles and policies for ECS services and instances

## Required Inputs

 - `cluster` - Name of ecs cluster, ex: Doorman, IdP, etc.

## Outputs

 - `ecs_cluster_name` - The ECS cluster name
 - `ecs_instance_role_id` - The ID for created IAM role `ecsInstanceRole`
 - `ecs_instance_profile_id` - The ID for created IAM profile `ecsInstanceProfile`
 - `ecs_instance_profile_arn` - The ARN for the created IAM profile `ecsInstanceProfile`
 - `ecs_service_role_id` - The ID for created IAM role `ecsServiceRole`

## Usage Example

```hcl
module "ecscluster" {
  source = "git::https://gitlab.com/millardtech/terraform_modules.git//aws/ecs_cluster"
  cluster = "${var.app_name}"
}
```