/*
 * Create ECS IAM Service Role and Policy
 * This is needed by an aws_ecs_service if used with a load balancer
 * The aws_ecs_service will need the ARN of the IAM role that allows Amazon ECS to make calls to your load balancer
 * on your behalf. This parameter is required if you are using a load balancer with your service, but only if your
 * task definition does not use the awsvpc network mode. If using awsvpc network mode, do not specify this role.
 * If your account has already created the Amazon ECS service-linked role, that role is used by default for your
 * service unless you specify a role here.
 */
resource "aws_iam_role" "ecsServiceRole" {
  count              = var.create_service_role ? 1 : 0
  name               = "ecsServiceRole-${random_id.code.hex}"
  assume_role_policy = var.ecsServiceRoleAssumeRolePolicy
}

resource "aws_iam_role_policy" "ecsServiceRolePolicy" {
  count  = var.create_service_role ? 1 : 0
  name   = "ecsServiceRolePolicy-${random_id.code.hex}"
  role   = aws_iam_role.ecsServiceRole[count.index].id
  policy = var.ecsServiceRolePolicy
}