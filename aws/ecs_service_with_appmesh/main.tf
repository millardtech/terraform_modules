/*
 * Get task definition data
 */
data "aws_ecs_task_definition" "td" {
  task_definition = aws_ecs_task_definition.td.family
  depends_on      = [aws_ecs_task_definition.td]
}

/*
 * Create task definition
 */
resource "aws_ecs_task_definition" "td" {
  family = "${var.service_name}-${var.service_env}"
  container_definitions = var.container_def_json
  task_role_arn = var.task_role_arn
  execution_role_arn = var.execution_role_arn
  network_mode = var.network_mode
  cpu = var.service_cpu
  memory = var.service_memory
  requires_compatibilities = var.requires_compatibilities

  dynamic "proxy_configuration" {
    for_each = var.use_proxy == false ? [] : [1]
    content {
      type = "APPMESH"
      container_name = "envoy"
      properties = {
        AppPorts = var.application_container_port
        EgressIgnoredIPs = "169.254.170.2,169.254.169.254"
        IgnoredUID = "1337"
        ProxyEgressPort = 15001
        ProxyIngressPort = 15000
      }
    }
  }
}

/*
 * Create ECS Service
 */
resource "aws_ecs_service" "service" {
  name                 = var.service_name
  cluster              = var.cluster_id
  desired_count        = var.desired_count
  force_new_deployment = var.force_new_deployment

  launch_type = length(var.capacity_provider_strategy) == 0 ? "FARGATE" : null

  dynamic "capacity_provider_strategy" {
    for_each = var.capacity_provider_strategy
    content {
      capacity_provider = capacity_provider_strategy.value.capacity_provider
      weight = capacity_provider_strategy.value.weight
      base = lookup(capacity_provider_strategy.value, "base", null)
    }
  }

  dynamic "service_registries" {
    for_each = var.service_registry_arn == "" ? [] : [1]
    content {
      registry_arn   = var.service_registry_arn
      container_name = var.service_full_name
    }
  }

  deployment_maximum_percent         = var.deployment_maximum_percent
  deployment_minimum_healthy_percent = var.deployment_minimum_healthy_percent

  dynamic "ordered_placement_strategy" {
    for_each = var.requires_compatibilities == "FARGATE" ? [] : [1]
    content {
      type = "spread"
      field = "instanceId"
    }
  }

  dynamic "load_balancer" {
    for_each = var.lb_attached ? [1] : []
    content {
      target_group_arn = var.lb_target_group_arn
      container_name = var.container_name
      container_port = var.application_container_port
    }
  }

  network_configuration {
    subnets = var.subnets
    security_groups = var.security_groups
    // avoid NAT for Now. this is needed for APPmesh and cloudwatch logs
    assign_public_ip = var.public_ip
  }

  # Track the latest ACTIVE revision
  task_definition = "${aws_ecs_task_definition.td.family}:${max(
    aws_ecs_task_definition.td.revision,
    data.aws_ecs_task_definition.td.revision,
  )}"

  tags = merge(
    var.additional_tags,
    {
      environment   = var.service_env,
      cpu           = var.service_cpu,
      memory        = var.service_memory,
      port          = var.application_container_port,
      external_name = var.service_name,
      registry      = var.service_registry_arn
    },
  )
}
