/*
 * Required Variables
 */
variable "cluster_id" {
  type = string
}

variable "service_name" {
  type = string
}

variable "service_env" {
  type = string
}

variable "container_def_json" {
  type = string
}

variable "desired_count" {
  type = string
}

variable "application_container_port" {
  type = number
  description = "Port the container app listens for traffic"
  default = "5000"
}

variable "network_mode" {
  type    = string
  default = "awsvpc"
}

variable "subnets" {
  type = list(string)
  description = "Subnets for the task to live in"
}

variable "security_groups" {
  type = list(string)
  description = "List of security groups for the service"
}

/*
 * Optional Variables
 */

variable "service_registry_arn" {
  default     = ""
  description = "ARN of aws_service_discovery_service resource"
  type        = string
}

variable "service_full_name" {
  type = string
  description = "required if service_registry is defined"
  default = ""
}

variable "capacity_provider_strategy" {
  type        = list(any)
  description = "(Optional) The capacity_provider_strategy configuration block. This is a list of maps, where each map should contain \"capacity_provider \", \"weight\" and \"base\""
  default     = []
}

variable "requires_compatibilities" {
  description = "Task Compatibilities, EC2 or FARGATE"
  type = set(string)
  default = ["EC2"]
}

variable "task_health_check" {
  type        = object({ command = list(string), interval = number, timeout = number, retries = number, startPeriod = number })
  description = "An optional healthcheck definition for the task"
  default     = null
}

variable "execution_role_arn" {
  type    = string
  description = "ARN of the task execution role that the Amazon ECS container agent and the Docker daemon can assume. Required if using private registry Auth"
  default = ""
}

variable "task_role_arn" {
  type    = string
  description = "ARN of IAM role that allows your Amazon ECS container task to make calls to other AWS services."
  default = ""
}

variable "deployment_maximum_percent" {
  type    = string
  default = 200
}

variable "deployment_minimum_healthy_percent" {
  type    = string
  default = 50
}

variable "force_new_deployment" {
  type        = bool
  description = "Enable to force a new task deployment of the service. This can be used to update tasks to use a newer Docker image with same image/tag combination (e.g. myimage:latest), roll Fargate tasks onto a newer platform version."
  default     = false
}

variable "public_ip" {
  type = bool
  description = "Assign a public IP to the task ENI(Fargate Launch type only)"
  default = false
}

variable "service_cpu" {
  default = null
  type = number
}

variable "service_memory" {
  default = null
  type = number
}

variable "additional_tags" {
  default     = {}
  description = "Additional resource tags"
  type        = map(string)
}

variable "use_proxy" {
  default = true
  description = "insert proxy block in task definition for appmesh"
}

variable "lb_attached" {
  type = bool
  default = false
  description = "If true provide lb_target_group_arn and container_name"
}

variable "container_name" {
  default = ""
  type = string
}

variable "lb_target_group_arn" {
  type = string
  default = ""
  description = "Provide lb target grp arn if lb_attached==true"
}
