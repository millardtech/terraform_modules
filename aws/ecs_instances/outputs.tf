output "asg_arn" {
  value = aws_autoscaling_group.asg.arn
}

output "capacity_provider_name" {
  value = aws_ecs_capacity_provider.ec2.name
}