# You can have multiple ECS clusters in the same account with different resources.
# Therefore all resources created here have a name containing the name of the:
# environment, cluster name, and the instance_group name.
# That is also the reason why ecs_instances is a separate module and not everything is created here.

# Default disk size for Docker is 22 gig, see http://docs.aws.amazon.com/AmazonECS/latest/developerguide/ecs-optimized_AMI.html
resource "aws_launch_template" "launch" {
  name_prefix   = "${var.environment}_${var.cluster}_${var.instance_group}_"
  image_id      = var.aws_ami != "" ? var.aws_ami : data.aws_ami.latest_ecs_ami.image_id
  instance_type = var.instance_type
  block_device_mappings {
    device_name = "/dev/sda1"
    ebs {
      volume_size = var.root_volume_size
    }
  }
  network_interfaces {
    associate_public_ip_address = var.public_ip_address
    security_groups             = var.security_groups
    delete_on_termination       = true
  }
  user_data = var.user_data
  iam_instance_profile {
    arn = var.instance_profile_arn
  }
  lifecycle {
    create_before_destroy = true
  }
  key_name = var.key_name
}

# Instances are scaled across availability zones http://docs.aws.amazon.com/autoscaling/latest/userguide/auto-scaling-benefits.html
resource "aws_autoscaling_group" "asg" {
  name_prefix               = "${var.environment}_${var.cluster}_${var.instance_group}"
  max_size                  = var.max_size
  min_size                  = var.min_size
  desired_capacity          = var.desired_capacity
  force_delete              = true
  vpc_zone_identifier       = var.subnet_ids
  default_cooldown          = var.cooldown_heath
  health_check_grace_period = var.cooldown_heath
  mixed_instances_policy {
    instances_distribution {
      on_demand_base_capacity  = var.on_demand_base_capacity
      spot_allocation_strategy = "lowest-price"
    }
    launch_template {
      launch_template_specification {
        launch_template_id = aws_launch_template.launch.id
        version            = "$Latest"
      }

      override {
        instance_type = "t3a.micro"
      }
      override {
        instance_type = "t3.micro"
      }
    }
  }
  tag {
    key                 = "Name"
    value               = "${var.environment}_ecs_${var.cluster}_${var.instance_group}"
    propagate_at_launch = "true"
  }

  tag {
    key                 = "Environment"
    value               = var.environment
    propagate_at_launch = "true"
  }

  tag {
    key                 = "Cluster"
    value               = var.cluster
    propagate_at_launch = "true"
  }

  tag {
    key                 = "InstanceGroup"
    value               = var.instance_group
    propagate_at_launch = "true"
  }

  # EC2 instances require internet connectivity to boot. Thus EC2 instances must not start before NAT is available.
  # For info why see description in the network module.
  tag {
    key                 = "DependsId"
    value               = var.depends_id
    propagate_at_launch = "false"
  }
}

resource "aws_ecs_capacity_provider" "ec2" {
  name = "${var.environment}_${var.cluster}_${var.instance_group}"
  auto_scaling_group_provider {
    auto_scaling_group_arn = aws_autoscaling_group.asg.arn
    managed_scaling {
      status          = "ENABLED"
      target_capacity = 100
    }
  }
}

