locals {
  dotted_name           = "${var.service_host_name}.${var.service_host_zone}"
  friendly_zone_name    = replace(var.service_host_zone, ".", "-")
  friendly_service_name = "${var.service_host_name}-${local.friendly_zone_name}"
}

resource "aws_appmesh_virtual_service" "virtual_service" {
  mesh_name = var.mesh_name
  name      = local.dotted_name

  spec {
    provider {
      virtual_router {
        virtual_router_name = aws_appmesh_virtual_router.virtual_router.name
      }
    }
  }
}

resource "aws_appmesh_virtual_router" "virtual_router" {
  mesh_name = var.mesh_name
  name      = "${local.friendly_service_name}-default-virtual-router"
  spec {
    listener {
      port_mapping {
        port     = var.client_port
        protocol = var.client_protocol
      }
    }
  }
}

resource "aws_appmesh_route" "http_default" {
  mesh_name           = var.mesh_name
  name                = "${local.friendly_service_name}-default-http-route"
  virtual_router_name = aws_appmesh_virtual_router.virtual_router.name

  spec {
    http_route {
      match {
        prefix = "/"
      }

      action {
        weighted_target {
          virtual_node = aws_appmesh_virtual_node.virtual_node.name
          weight       = 100
        }
      }

      retry_policy {
        http_retry_events = [
          "server-error",
        ]
        max_retries = var.max_retries
        per_retry_timeout {
          unit  = "s"
          value = var.retry_timeout_seconds
        }
      }

    }
  }
}

resource "aws_service_discovery_service" "service" {
  name = var.service_host_name
  namespace_id = var.sds_namespace_id
}

resource "aws_appmesh_virtual_node" "virtual_node" {
  mesh_name = var.mesh_name
  name      = local.friendly_service_name

  spec {
    # This is the listener that the "virtual node" e.g. service instances present to Envoy.
    # Not the listener that Envoy presents to the meshed service.
    listener {
      port_mapping {
        port     = var.target_service_listener_port
        protocol = var.target_service_listener_protocol
      }

      health_check {
        protocol            = "http"
        path                = var.target_service_healthcheck_path
        healthy_threshold   = 2
        unhealthy_threshold = 2
        timeout_millis      = var.health_check_timeout_millis
        interval_millis     = var.health_check_interval_millis
        port                = var.target_service_listener_port
      }
    }

    service_discovery {
      aws_cloud_map {
        namespace_name = var.service_host_zone
        service_name = var.service_host_name
      }
    }

    logging {
      access_log {
        file {
          path = "/dev/stdout"
        }
      }
    }
  }
}