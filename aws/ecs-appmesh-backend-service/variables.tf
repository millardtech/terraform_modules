variable "mesh_name" {
  type = string
}
variable "service_host_name" {
  type = string
}
variable "service_host_zone" {
  type = string
}

variable "sds_namespace_id" {
  description = "The Service discovery namespace that the virtual node should reference"
  type = string
}

variable "client_port" {
  description = "The port present to clients on which to connect, which envoy is listening on"
  type        = number
  default     = 80
}
variable "client_protocol" {
  description = "The protocol present to clients on which to connect"
  type        = string
  default     = "http"
}

variable "target_service_listener_port" {
  description = "The port that the real target service listens on. e.g Flask is 5000, which envoy redirects traffic"
  type        = number
  default     = 80
}

variable "target_service_listener_protocol" {
  description = "The protocol that the real target service listens on."
  type        = string
  default     = "http"
}

variable "max_retries" {
  type    = number
  default = 4
}
variable "retry_timeout_seconds" {
  type    = number
  default = 5
}

variable "target_service_healthcheck_path" {
  type = string
}
variable "health_check_timeout_millis" {
  type    = number
  default = 5000
}
variable "health_check_interval_millis" {
  type    = number
  default = 5000
}
