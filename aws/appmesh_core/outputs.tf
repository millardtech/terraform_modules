output "mesh_name" {
  value = aws_appmesh_mesh.primary.name
}

output "service_discovery_namespace" {
  value = {
    namespace_name = aws_service_discovery_http_namespace.service_discovery_namespace_node_internal.name
    namespace_id   = aws_service_discovery_http_namespace.service_discovery_namespace_node_internal.id
    namespace_arn  = aws_service_discovery_http_namespace.service_discovery_namespace_node_internal.arn
  }
}
