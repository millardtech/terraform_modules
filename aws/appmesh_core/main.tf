# To enable appmesh, we need an appmesh mesh and a cloudmap zone.

resource "aws_appmesh_mesh" "primary" {
  name = var.mesh_name

  # enable outbound connections
  spec {
    egress_filter {
      type = var.egress_filter
    }
  }

  tags = {
    Name = var.mesh_name
    Environment = var.environment
  }
}

# DISCOVERY SERVICE NAMESPACES
# public
resource "aws_service_discovery_http_namespace" "service_discovery_namespace_node_internal" {
  name        = "node.internal"
  description = "A service discovery http namespace for node.internal"
}
