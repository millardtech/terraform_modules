variable "environment" {}

variable "mesh_name" {
  type = string
}

variable "egress_filter" {
  description = "Enable outbound connections. Valid values are ALLOW_ALL and DROP_ALL"
  type = string
  default = "DROP_ALL"
}
