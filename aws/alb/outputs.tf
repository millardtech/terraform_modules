output "target_group_arn" {
  value = aws_lb_target_group.default.arn
}

output "arn" {
  value = aws_lb.alb.arn
}

output "dns_name" {
  value = aws_lb.alb.dns_name
}

output "https_listener_arn" {
  value = aws_alb_listener.https.arn
}
