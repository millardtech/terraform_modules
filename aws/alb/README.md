# aws/alb - Application Load Balancer
This module is used to create an application load balancer along with security
groups for traffic and a default target group.

## What this does

 - Create LB named after `lb_name`
 - Create LB target group
 - Create HTTPS listener for LB / Target Group

## Required Inputs

 - `lb_name` - Name of the load balancer, ex: Doorman, IdP, etc.
 - `environment` - Name of environment, ex: prod, test, etc.
 - `vpc_id` - ID of VPC for target group.
 - `security_groups` - List of security groups to apply to ALB.
 - `subnets` - A list of public subnet ids for ALB placement
 - `certificate_arn` - ARN to SSL certificate to use with HTTPS listener

### Optional Inputs
 - `port` - Target group listening port. Default: `80`
 - `protocol` - Target group listening protocol. Default: `http`
 - `health_check_interval` - Default: `30`
 - `health_check_path` - Default: `/`
 - `health_check_port` - Default: `traffic-port`
 - `health_check_protocol` - Default: `HTTP`
 - `health_check_timeout` - Default: `10`
 - `healthy_threshold` - Default: `3`
 - `unhealthy_threshold` - Default: `3`
 - `health_check_status_codes` - Default: `200`, separate multiple values with comma, ex: `200,204`
 - `idle_timeout` - Default: `60`
 - `load_balancer_type` - Options: `application`, `gateway` or `network`. Default: `application`

## Outputs

 - `arn` - ARN for ALB
 - `dns_name` - DNS hostname for ALB
 - `target_group_arn` - ARN for Target Group
 - `https_listener_arn` - ARN for https listener

## Example Usage

```hcl
module "asg" {
  source = "git::https://gitlab.com/millardtech/terraform_modules.git//aws/alb"
  lb_name = "${var.app_name}"
  environment = "${var.app_env}"
  vpc_id = "${module.vpc.id}"
  security_groups = ["${module.vpc.vpc_default_sg_id}"]
  subnets = ["${module.vpc.public_subnet_ids}"]
  certificate_arn = "${data.aws_acm_certificate.name.arn}"
}
```