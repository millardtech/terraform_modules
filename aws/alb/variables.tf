variable "vpc_id" {
  type = string
}

variable "lb_name" {
  type = string
  description = "LB name. Max 32 characters"
}

variable "environment" {
  type = string
}

variable "security_groups" {
  type = list(string)
}

variable "subnets" {
  type = list(string)
}

variable "certificate_arn" {
  type = string
}


/*
 * Optional variables
 */

variable "port" {
  type    = string
  default = 80
  description = "Port on which targets receive traffic."
}

variable "protocol" {
  type    = string
  default = "HTTP"
  description = "GENEVE, HTTP, HTTPS, TCP, TCP_UDP, TLS, or UDP"
}

variable "internal" {
  type    = bool
  default = false
  description = "Is this load-balancer internal?"
}

variable "ssl_policy" {
  type    = string
  default = "ELBSecurityPolicy-2016-08"
}

variable "load_balancer_type" {
  description = "application, gateway, or network"
  default = "application"
  type = string
}

variable "target_type" {
  default = "instance"
  description = "instance, ip or lambda"
}

variable "health_check_interval" {
  default = "30"
}

variable "health_check_path" {
  default = "/"
}

variable "health_check_port" {
  default = "traffic-port"
}

variable "health_check_protocol" {
  default = "HTTP"
  description = "Protocol to use when connecting to targets. NA when target_type=lambda"
}

variable "health_check_timeout" {
  default = "10"
}

variable "healthy_threshold" {
  default = "3"
}

variable "unhealthy_threshold" {
  default = "3"
}

variable "health_check_status_codes" {
  default = "200"
  description = "matcher for healthcheck. NA when protocol = TCP"
}

variable "idle_timeout" {
  default = "60"
  description = "Seconds that the connection is allowed to be idle. Only valid with application load balancers"
}

variable "tg_name" {
  type        = string
  default     = ""
  description = "Manual override for ALB Target Group name (which is otherwise assembled alb_name)"
}
