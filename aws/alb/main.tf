/*
 * Create application load balancer
 */
resource "aws_lb" "alb" {
  name               = var.lb_name
  internal           = var.internal
  security_groups    = var.security_groups
  subnets            = var.subnets
  load_balancer_type = var.load_balancer_type
  idle_timeout       = var.idle_timeout

  tags = {
    Name        = var.lb_name
    Environment = var.environment
  }
}

/*
 * Create target group for ALB
 */
resource "aws_lb_target_group" "default" {
  name     = coalesce(var.tg_name, "tg-${var.lb_name}")
  port     = var.port
  protocol = var.protocol
  vpc_id   = var.vpc_id
  target_type = var.target_type

  // https://github.com/hashicorp/terraform-provider-aws/pull/9587 bug
//  health_check {
//    interval            = var.health_check_interval
//    path                = var.health_check_path
//    port                = var.health_check_port
//    protocol            = var.health_check_protocol
//    timeout             = var.health_check_timeout
//    healthy_threshold   = var.healthy_threshold
//    unhealthy_threshold = var.unhealthy_threshold
//    matcher             = var.health_check_status_codes
//  }
}

/*
 * Create listeners to connect ALB to target group
 */
resource "aws_alb_listener" "https" {
  load_balancer_arn = aws_lb.alb.arn
  port              = var.port
  protocol          = var.load_balancer_type == "application" ? "HTTPS" : "TCP"
  ssl_policy        = var.load_balancer_type == "application" ? var.ssl_policy : null
  certificate_arn   = var.certificate_arn

  default_action {
    target_group_arn = aws_lb_target_group.default.arn
    type             = "forward"
  }
}

// Not valid for Network LB
//resource "aws_lb_listener" "plain_redirect" {
//  load_balancer_arn = aws_lb.alb.arn
//  port              = "80"
//  protocol          = var.load_balancer_type == "application" ? "HTTPS" : "TCP"
//
//  default_action {
//    type = "redirect"
//
//    redirect {
//      port        = "443"
//      protocol    = "HTTPS"
//      status_code = "HTTP_301"
//    }
//  }
//}
